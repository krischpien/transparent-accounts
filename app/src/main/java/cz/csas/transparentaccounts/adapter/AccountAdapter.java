package cz.csas.transparentaccounts.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cz.csas.transparentaccounts.R;
import cz.csas.transparentaccounts.model.entity.BankAccount;

public class AccountAdapter
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_BANK_ACCOUNT = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private List<BankAccount> bankAccountList = new ArrayList<>();
    private OnBankAccountClickListener onBankAccountClickListener = null;

    @Override
    public int getItemViewType(int position) {
        return bankAccountList.get(position) != null ? VIEW_TYPE_BANK_ACCOUNT : VIEW_TYPE_LOADING;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case VIEW_TYPE_BANK_ACCOUNT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_bank_account, parent, false);
                return new BankAccountViewHolder(view);

            case VIEW_TYPE_LOADING:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_loading, parent, false);
                return new LoadingViewHolder(view);

            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof BankAccountViewHolder){

            BankAccount bankAccount = bankAccountList.get(position);
            BankAccountViewHolder bankAccountHolder = ((BankAccountViewHolder) holder);
            Context context = bankAccountHolder.rootView.getContext();
            bankAccountHolder.bankAccount = bankAccount;
            String bankAccountString = context.getString(
                    R.string.account_detail_number, bankAccount.getAccountNumber(), bankAccount.getBankCode());
            String ibanString = context.getString(R.string.account_detail_iban, bankAccount.getIban());
            bankAccountHolder.bankAccountText.setText(bankAccountString);
            bankAccountHolder.ibanText.setText(ibanString);
            if (onBankAccountClickListener != null) {
                bankAccountHolder.rootView.setOnClickListener(
                        v -> onBankAccountClickListener.onBankAccountClick(bankAccount)
                );
            }
        } else if(holder instanceof LoadingViewHolder){
            // nothing to bind, just showing progressbar
        }

    }


    public void removeBankAccount(int position){
        if(bankAccountList.isEmpty()){
            return;
        } else {
            bankAccountList.remove(position);
        }
    }

    @Override
    public int getItemCount() {
        return bankAccountList.size();
    }

    public OnBankAccountClickListener getOnBankAccountClickListener() {
        return onBankAccountClickListener;
    }

    public void setOnBankAccountClickListener(OnBankAccountClickListener onBankAccountClickListener) {
        this.onBankAccountClickListener = onBankAccountClickListener;
    }

    public boolean addBankAccount(BankAccount bankAccount) {
        return bankAccountList.add(bankAccount);
    }

    public boolean addBankAccountAndNotifyItemInserted(BankAccount bankAccount) {
        boolean accountAdded = addBankAccount(bankAccount);
        notifyItemInserted(bankAccountList.size());
        return accountAdded;
    }


    public boolean setBankAccounts(List<BankAccount> bankAccountList) {
        this.bankAccountList.clear();
        return this.bankAccountList.addAll(bankAccountList);
    }

    public boolean setBankAccountsAndNotifyDataSetChanged(List<BankAccount> bankAccountList) {
        boolean accountListAdded = this.setBankAccounts(bankAccountList);
        notifyDataSetChanged();
        return accountListAdded;
    }


    public boolean addBankAccountListAndNotifyDataSetChanged(List<BankAccount> bankAccountList){
        boolean accountListAdded = this.bankAccountList.addAll(bankAccountList);
        notifyDataSetChanged();
        return accountListAdded;
    }

    public List<BankAccount> getBankAccountList() {
        return bankAccountList;
    }

    public void setBankAccountList(List<BankAccount> bankAccountList) {
        this.bankAccountList = bankAccountList;
    }

    public static class BankAccountViewHolder extends RecyclerView.ViewHolder {
        View rootView;
        TextView bankAccountText;
        TextView ibanText;
        BankAccount bankAccount;

        public BankAccountViewHolder(View view) {
            super(view);
            rootView = view;
            bankAccountText = (TextView) view.findViewById(R.id.bank_account);
            ibanText = (TextView) view.findViewById(R.id.bank_account_iban);
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.item_progress_bar);
        }
    }

    public interface OnBankAccountClickListener {
        void onBankAccountClick(BankAccount bankAccount);
    }

}