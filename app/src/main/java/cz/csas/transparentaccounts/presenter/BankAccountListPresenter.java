package cz.csas.transparentaccounts.presenter;

import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import cz.csas.transparentaccounts.adapter.AccountAdapter;
import cz.csas.transparentaccounts.model.dto.BankAccountsResponse;
import cz.csas.transparentaccounts.model.entity.BankAccount;
import cz.csas.transparentaccounts.view.BankAccountListView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class BankAccountListPresenter extends BasePresenter{

    private BankAccountListView view;
    private AccountAdapter adapter = new AccountAdapter();

    private boolean isLoading = false;
    private static final int VISIBLE_THRESHOLD = 10;
    private int visibleItemCount = 0;
    private int firstVisibleItem = 0;
    private int totalItemCount = 0;
    private int currentPage = 0;
    private boolean canLoadNextPage = true;


    public BankAccountListPresenter(BankAccountListView view) {
        this.view = view;
    }

    public void loadBankAccountList(int pageNumber){
        currentPage = pageNumber;
        if(pageNumber == 0){
            // show main ('full-screen') loading progress bar
            view.showLoading(true);
        }
        if(canLoadNextPage){
            getBankAccountService().getAllAccounts(pageNumber)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onBankAccountsLoaded, this::onError);
        }
    }

    private void onError(Throwable throwable) {
        Timber.e(throwable.getMessage());
        view.showLoading(false);
    }

    public void appendScrollLoadingListener(RecyclerView recyclerView){
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if(recyclerView.getAdapter() instanceof AccountAdapter){
            adapter = (AccountAdapter) recyclerView.getAdapter();
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                visibleItemCount = linearLayoutManager.getChildCount();
                if (!isLoading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + VISIBLE_THRESHOLD)) {
                    if(VISIBLE_THRESHOLD > totalItemCount) return;
                    // End has been reached

                    // load next page (if there's any)
                    if(canLoadNextPage){
                        isLoading = true;
                        currentPage++;

                        showLoadingView();
                        loadBankAccountList(currentPage);
                    }
                }
            }
        });
    }

    private void showLoadingView() {
        new Handler().post(() -> {
            adapter.addBankAccountAndNotifyItemInserted(null);
        });
    }

    private void hideLoadingView(){
        new Handler().post(() -> {
            int itemCount = adapter.getItemCount();
            List<BankAccount> bankAccountList = adapter.getBankAccountList();
            bankAccountList.remove(null);
            adapter.notifyItemRemoved(itemCount);
        });

    }

    private void onBankAccountsLoaded(BankAccountsResponse response) {
        hideLoadingView();
        isLoading = false;
        view.showLoading(false);
        canLoadNextPage = response.getPageCount() > response.getPageNumber() + 1;
        view.renderBankAccountList(response.getBankAccounts());
    }

}
