package cz.csas.transparentaccounts.presenter;

import cz.csas.transparentaccounts.model.dto.BankAccountTransactionsResponse;
import cz.csas.transparentaccounts.model.entity.BankAccount;
import cz.csas.transparentaccounts.view.BankAccountDetailView;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by jankristdev@gmail.com on 1.10.2016.
 */

public class BankAccountDetailPresenter extends BasePresenter{
    private BankAccountDetailView detailView = null;
    private Subscription subscription;


    public BankAccountDetailPresenter(BankAccountDetailView detailView) {
        this.detailView = detailView;
    }

    public void loadAccountDetail(String accountId){
        detailView.showLoading(true);
        subscription = getBankAccountService().getAccountById(accountId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                // could/should be replaced by composite subscription
                .flatMap(new Func1<BankAccount, Observable<BankAccountTransactionsResponse>>() {
                    @Override
                    public Observable<BankAccountTransactionsResponse> call(BankAccount bankAccount) {
                        onAccountLoaded(bankAccount);
                        return getBankAccountService().getTransactionForAccount(bankAccount.getAccountNumber())
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread());
                    }
                })
                .subscribe(this::onTransactionLoaded, this::onError);
    }

    private void onAccountLoaded(BankAccount bankAccount) {
        detailView.renderBankAccount(bankAccount);

    }

    private void onTransactionLoaded(BankAccountTransactionsResponse response) {
        detailView.showLoading(false);
        detailView.renderTransactions(response.getBankAccountTransactionList());
    }

    private void onError(Throwable throwable) {
        Timber.e(throwable.getMessage());
        detailView.showLoading(false);
    }

    public void onStop(){
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }


}
