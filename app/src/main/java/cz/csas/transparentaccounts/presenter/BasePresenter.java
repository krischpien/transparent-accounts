package cz.csas.transparentaccounts.presenter;

import cz.csas.transparentaccounts.BankApplication;
import cz.csas.transparentaccounts.model.dao.BankAccountService;
import cz.csas.transparentaccounts.model.dao.mock.MockAccountService;
import cz.csas.transparentaccounts.model.dao.retrofit.RetrofitServiceProvider;

/**
 * Created by jankristdev@gmail.com on 01.10.2016.
 */

public abstract class BasePresenter {

    private int appMode = BankApplication.MODE_PRODUCTION;

    private BankAccountService bankAccountService = RetrofitServiceProvider.getBankAccountServiceInstance();

    public void setAppMode(int appMode) {
        this.appMode = appMode;
        switch (appMode){
            case BankApplication.MODE_MOCK:
                bankAccountService = new MockAccountService();
                break;
            case BankApplication.MODE_PRODUCTION:
                bankAccountService = RetrofitServiceProvider.getBankAccountServiceInstance();
                break;
        }
    }

    public int getAppMode() {
        return appMode;
    }

    protected BankAccountService getBankAccountService(){
        return bankAccountService;
    }
}
