package cz.csas.transparentaccounts.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by jankristdev@gmail.com on 01.10.2016.
 */

public class TimeUtils {
    public static final String DEFAULT_DATE_FORMAT_INPUT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DEFAULT_DATE_FORMAT_OUTPUT = "d. M. yyyy";

    public static String formatDate(String dateInputString){

        DateTimeFormatter inputFormatter = DateTimeFormat.forPattern(DEFAULT_DATE_FORMAT_INPUT);
        DateTime dateTime = inputFormatter.parseDateTime(dateInputString);
        DateTimeFormatter outputFormater = DateTimeFormat.forPattern(DEFAULT_DATE_FORMAT_OUTPUT);
        String dateOutputString = dateTime.toString(outputFormater);
        return dateOutputString;
    }
}
