package cz.csas.transparentaccounts.model.dao;

import cz.csas.transparentaccounts.model.dto.BankAccountTransactionsResponse;
import cz.csas.transparentaccounts.model.dto.BankAccountsResponse;
import cz.csas.transparentaccounts.model.entity.BankAccount;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by jankristdev@gmail.com on 1.10.2016.
 */

public interface BankAccountService {

    @GET("transparentAccounts")
    Observable<BankAccountsResponse> getAllAccounts(@Query("pageNumber") int pageNumber);

    @GET("transparentAccounts/{bankAccountId}")
    Observable<BankAccount> getAccountById(@Path("bankAccountId") String bankAccountId);

    @GET("transparentAccounts/{bankAccountId}/transactions")
    Observable<BankAccountTransactionsResponse> getTransactionForAccount(@Path("bankAccountId") String bankAccountId);
}
