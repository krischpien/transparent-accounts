package cz.csas.transparentaccounts.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by jankristdev@gmail.com on 01.10.2016.
 */

public class BankAccount implements Serializable, Parcelable {
    private String accountNumber;
    private String bankCode;
    private String transparencyFrom;
    private String transparencyTo;
    private String publicationTo;
    private String actualizationDate;
    private double balance;
    private String currency;
    private String iban;

    public BankAccount() {
    }

    public BankAccount(String accountNumber, String bankCode) {
        this.accountNumber = accountNumber;
        this.bankCode = bankCode;
    }

    public BankAccount(String accountNumber, String bankCode, String iban) {
        this.accountNumber = accountNumber;
        this.bankCode = bankCode;
        this.iban = iban;
    }

    public BankAccount(String accountNumber, String bankCode, String transparencyFrom, String transparencyTo) {
        this.accountNumber = accountNumber;
        this.bankCode = bankCode;
        this.transparencyFrom = transparencyFrom;
        this.transparencyTo = transparencyTo;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getTransparencyFrom() {
        return transparencyFrom;
    }

    public void setTransparencyFrom(String transparencyFrom) {
        this.transparencyFrom = transparencyFrom;
    }

    public String getTransparencyTo() {
        return transparencyTo;
    }

    public void setTransparencyTo(String transparencyTo) {
        this.transparencyTo = transparencyTo;
    }

    public String getPublicationTo() {
        return publicationTo;
    }

    public void setPublicationTo(String publicationTo) {
        this.publicationTo = publicationTo;
    }

    public String getActualizationDate() {
        return actualizationDate;
    }

    public void setActualizationDate(String actualizationDate) {
        this.actualizationDate = actualizationDate;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.accountNumber);
        dest.writeString(this.bankCode);
        dest.writeString(this.transparencyFrom);
        dest.writeString(this.transparencyTo);
        dest.writeString(this.publicationTo);
        dest.writeString(this.actualizationDate);
        dest.writeDouble(this.balance);
        dest.writeString(this.currency);
        dest.writeString(this.iban);
    }

    protected BankAccount(Parcel in) {
        this.accountNumber = in.readString();
        this.bankCode = in.readString();
        this.transparencyFrom = in.readString();
        this.transparencyTo = in.readString();
        this.publicationTo = in.readString();
        this.actualizationDate = in.readString();
        this.balance = in.readDouble();
        this.currency = in.readString();
        this.iban = in.readString();
    }

    public static final Parcelable.Creator<BankAccount> CREATOR = new Parcelable.Creator<BankAccount>() {
        @Override
        public BankAccount createFromParcel(Parcel source) {
            return new BankAccount(source);
        }

        @Override
        public BankAccount[] newArray(int size) {
            return new BankAccount[size];
        }
    };

    @Override
    public String toString() {
        return "BankAccount{" +
                "accountNumber='" + accountNumber + '\'' +
                ", bankCode='" + bankCode + '\'' +
                ", transparencyFrom='" + transparencyFrom + '\'' +
                ", transparencyTo='" + transparencyTo + '\'' +
                ", publicationTo='" + publicationTo + '\'' +
                ", actualizationDate='" + actualizationDate + '\'' +
                ", balance=" + balance +
                ", currency='" + currency + '\'' +
                ", iban='" + iban + '\'' +
                '}';
    }
}
