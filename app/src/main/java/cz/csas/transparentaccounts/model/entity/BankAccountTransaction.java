package cz.csas.transparentaccounts.model.entity;

import java.io.Serializable;

/**
 * Created by jankristdev@gmail.com on 1.10.2016.
 */

public class BankAccountTransaction implements Serializable {

    private Sender sender;
    private Receiver receiver;
    private Amount amount;

    private String type;
    private String dueDate;
    private String processingDate;

    public BankAccountTransaction() {
    }

    public BankAccountTransaction(Sender sender, Receiver receiver, Amount amount, String processingDate) {
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
        this.processingDate = processingDate;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getProcessingDate() {
        return processingDate;
    }

    public void setProcessingDate(String processingDate) {
        this.processingDate = processingDate;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public static class Amount {
        private double value;
        private int precision;
        private String currency;

        public Amount() {
        }

        public Amount(double value, String currency) {
            this.value = value;
            this.currency = currency;
        }

        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public int getPrecision() {
            return precision;
        }

        public void setPrecision(int precision) {
            this.precision = precision;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }

    @Override
    public String toString() {
        return "BankAccountTransaction{" +
                "amount=" + amount.getValue() +
                ", type='" + type + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", processingDate='" + processingDate + '\'' +
                '}';
    }
}
