package cz.csas.transparentaccounts.model.dao.mock;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cz.csas.transparentaccounts.model.dao.BankAccountService;
import cz.csas.transparentaccounts.model.dto.BankAccountTransactionsResponse;
import cz.csas.transparentaccounts.model.dto.BankAccountsResponse;
import cz.csas.transparentaccounts.model.entity.BankAccount;
import cz.csas.transparentaccounts.model.entity.BankAccountTransaction;
import cz.csas.transparentaccounts.model.entity.Receiver;
import cz.csas.transparentaccounts.model.entity.Sender;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by jankristdev@gmail.com on 1.10.2016.
 */

public class MockAccountService implements BankAccountService {

    public static final long LOADING_DELAY = 2000;

    @Override
    public Observable<BankAccountsResponse> getAllAccounts(int pageNumber) {
        BankAccountsResponse apiResponse = new BankAccountsResponse();
        apiResponse.setPageCount(4);
        apiResponse.setPageSize(10);
        List<BankAccount> bankAccountList = new ArrayList<>();

        switch (pageNumber){
            case 0:
                apiResponse.setPageNumber(0);
                bankAccountList.add(new BankAccount("0011-0123456-1234", "0800", "CZ13 0800 0000 0029 0647 4561"));
                bankAccountList.add(new BankAccount("1124-1156783-1334", "0800", "CZ14 0800 0000 0029 0647 4562"));
                bankAccountList.add(new BankAccount("0014-1245789-7894", "0800", "CZ13 0800 0000 0029 0647 4421"));
                bankAccountList.add(new BankAccount("0019-7899788-1237", "0800", "CZ16 0800 0000 0029 0647 9712"));
                bankAccountList.add(new BankAccount("0479-0123456-1234", "0800", "CZ13 0800 0000 0029 0647 1237"));
                bankAccountList.add(new BankAccount("0013-4789554-1334", "0800", "CZ13 0800 0000 0029 0647 9975"));
                bankAccountList.add(new BankAccount("0666-6549878-5534", "0800", "CZ18 0800 0000 0029 0647 5578"));
                bankAccountList.add(new BankAccount("0019-0123456-4561", "0800", "CZ13 0800 0000 0029 0647 6678"));
                bankAccountList.add(new BankAccount("0011-1235640-5547", "0800", "CZ19 0800 0000 0029 0647 8309"));
                bankAccountList.add(new BankAccount("0045-0123456-1334", "0800", "CZ13 0800 0000 0029 0647 8309"));
                break;
            case 1:
                apiResponse.setPageNumber(1);
                bankAccountList.add(new BankAccount("0788-1112364-1247", "0800", "CZ21 0800 0000 0029 0647 4578"));
                bankAccountList.add(new BankAccount("0011-0123456-1234", "0800", "CZ13 0800 0000 0029 0647 4561"));
                bankAccountList.add(new BankAccount("1124-1156783-1334", "0800", "CZ13 0800 0000 0029 0647 5678"));
                bankAccountList.add(new BankAccount("0014-1245789-7894", "0800", "CZ45 0800 0000 0029 0647 6879"));
                bankAccountList.add(new BankAccount("0019-7899788-1237", "0800", "CZ13 0800 0000 0029 0647 8309"));
                bankAccountList.add(new BankAccount("0479-0123456-1234", "0800", "CZ13 0800 0000 0029 0647 1234"));
                bankAccountList.add(new BankAccount("0013-4789554-1334", "0800", "CZ65 0800 0000 0029 0647 8309"));
                bankAccountList.add(new BankAccount("0666-6549878-5534", "0800", "CZ13 0800 0000 0029 0647 1245"));
                bankAccountList.add(new BankAccount("0019-0123456-4561", "0800", "CZ11 0800 0000 0029 0647 8309"));
                bankAccountList.add(new BankAccount("0011-1235640-5547", "0800", "CZ09 0800 0000 0029 0647 5555"));
                break;
            case 2:
                apiResponse.setPageNumber(2);
                bankAccountList.add(new BankAccount("0789-4567895-5534", "0800", "CZ13 0800 0000 0029 0647 4567"));
                bankAccountList.add(new BankAccount("0788-1112364-1247", "0800", "CZ55 0800 0000 0029 0647 8309"));
                bankAccountList.add(new BankAccount("0011-0123456-1234", "0800", "CZ21 0800 0000 0029 0647 1234"));
                bankAccountList.add(new BankAccount("1124-1156783-1334", "0800", "CZ13 0800 0000 0029 0647 8309"));
                bankAccountList.add(new BankAccount("0014-1245789-7894", "0800", "CZ48 0800 0000 0029 0647 7891"));
                bankAccountList.add(new BankAccount("0019-7899788-1237", "0800", "CZ66 0800 0000 0029 0647 8309"));
                bankAccountList.add(new BankAccount("0479-0123456-1234", "0800", "CZ12 0800 0000 0029 0647 4567"));
                bankAccountList.add(new BankAccount("0013-4789554-1334", "0800", "CZ11 0800 0000 0029 0647 8309"));
                bankAccountList.add(new BankAccount("0666-6549878-5534", "0800", "CZ19 0800 0000 0029 0647 4567"));
                bankAccountList.add(new BankAccount("0019-0123456-4561", "0800", "CZ20 0800 0000 0029 0647 8309"));
                break;
            case 3:
                apiResponse.setPageNumber(3);
                bankAccountList.add(new BankAccount("0045-0123456-1334", "0800", "CZ11 0800 0000 0029 0647 8309"));
                bankAccountList.add(new BankAccount("0789-4567895-5534", "0800", "CZ11 0800 0000 0029 0647 4562"));
                bankAccountList.add(new BankAccount("0788-1112364-1247", "0800", "CZ19 0800 0000 0029 0647 4567"));
                bankAccountList.add(new BankAccount("0789-4567895-5534", "0800", "CZ20 0800 0000 0029 0647 1234"));
                bankAccountList.add(new BankAccount("0045-0123456-1334", "0800", "CZ16 0800 0000 0029 0647 1245"));
                bankAccountList.add(new BankAccount("0011-1235640-5547", "0800", "CZ11 0800 0000 0029 0647 7894"));
                break;
        }

        apiResponse.setBankAccounts(bankAccountList);
        return Observable.just(apiResponse).delay(LOADING_DELAY, TimeUnit.MILLISECONDS);
    }

    @Override
    public Observable<BankAccount> getAccountById(String id) {
        BankAccount bankAccount = new BankAccount(id, "0800", "2016-10-01T00:00:00", "2050-08-18T00:00:00");
        bankAccount.setBalance(25005.65);
        bankAccount.setCurrency("CZK");
        return Observable.just(bankAccount).delay(LOADING_DELAY, TimeUnit.MILLISECONDS);
    }

    @Override
    public Observable<BankAccountTransactionsResponse> getTransactionForAccount(@Path("bankAccountId") String bankAccountId) {
        BankAccountTransactionsResponse response = new BankAccountTransactionsResponse();
        List<BankAccountTransaction> bankAccountTransactionList = new ArrayList<>();
        bankAccountTransactionList.add(new BankAccountTransaction(
                new Sender("1234567890", "0800"),
                new Receiver("0987654321", "0800"),
                new BankAccountTransaction.Amount(2465.54, "CZK"), "2016-10-01T00:00:00"));
        bankAccountTransactionList.add(new BankAccountTransaction(
                new Sender("1234567890", "0800"),
                new Receiver("0987654321", "0800"),
                new BankAccountTransaction.Amount(6523.45, "CZK"), "2016-10-01T00:00:00"));
        bankAccountTransactionList.add(new BankAccountTransaction(
                new Sender("1234567890", "0800"),
                new Receiver("0987654321", "0800"),
                new BankAccountTransaction.Amount(150.00, "USD"), "2016-10-01T00:00:00"));
        response.setPageSize(1);
        response.setBankAccountTransactionList(bankAccountTransactionList);
        return Observable.just(response).delay(500, TimeUnit.MILLISECONDS);
    }
}
