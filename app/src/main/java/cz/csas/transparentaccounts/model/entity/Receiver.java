package cz.csas.transparentaccounts.model.entity;

import java.io.Serializable;

/**
 * Created by jankristdev@gmail.com on 1.10.2016.
 */

public class Receiver implements Serializable {
    private String accountNumber;
    private String bankCode;
    private String iban;

    public Receiver() {
    }

    public Receiver(String accountNumber, String bankCode) {
        this.accountNumber = accountNumber;
        this.bankCode = bankCode;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }
}
