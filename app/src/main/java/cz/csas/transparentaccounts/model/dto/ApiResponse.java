package cz.csas.transparentaccounts.model.dto;

import java.io.Serializable;

/**
 * Created by jankristdev@gmail.com on 1.10.2016.
 */

public abstract class ApiResponse implements Serializable {

    private int pageNumber;
    private int pageCount;
    private int pageSize;
    private int recordCount;


    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }

}
