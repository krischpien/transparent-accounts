package cz.csas.transparentaccounts.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import cz.csas.transparentaccounts.model.entity.BankAccount;

/**
 * Created by jankristdev@gmail.com on 1.10.2016.
 */

public class BankAccountsResponse extends ApiResponse {

    @SerializedName("accounts")
    private List<BankAccount> bankAccounts;

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(List<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }
}
