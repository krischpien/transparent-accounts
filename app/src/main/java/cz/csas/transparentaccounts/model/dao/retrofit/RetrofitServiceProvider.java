package cz.csas.transparentaccounts.model.dao.retrofit;

import cz.csas.transparentaccounts.model.dao.BankAccountService;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jankristdev@gmail.com on 1.10.2016.
 */

public class RetrofitServiceProvider {

    private static final String CSAS_ENDPOINT = "https://api.csas.cz/sandbox/webapi/api/v2/";
    private static final String CSAS_WEB_API_KEY = "c32ac76d-eeea-4e84-801a-0e4ef35664a3";
    private static final String CSAS_WEB_API_KEY_PARAM_NAME = "WEB-API-key";

    private static OkHttpClient okHttpClient = null;
    private static Retrofit retrofit = null;
    private static BankAccountService bankAccountService = null;

    private RetrofitServiceProvider() {

    }

    private static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getOkHttpClientInstance())
                    .baseUrl(CSAS_ENDPOINT)
                    .build();
        }
        return retrofit;
    }

    private static OkHttpClient getOkHttpClientInstance() {
        if (okHttpClient == null) {
            // Interceptor for logging
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            // Interceptor for all requests (adding web-api-key query for every request)
            Interceptor requestInterceptor = chain -> {
                Request originalRequest = chain.request();
                HttpUrl originalHttpUrl = originalRequest.url();

                Request.Builder requestBuilder = originalRequest.newBuilder()
                        .header(CSAS_WEB_API_KEY_PARAM_NAME, CSAS_WEB_API_KEY)
                        .url(originalHttpUrl);

                Request interceptedRequest = requestBuilder.build();
                return chain.proceed(interceptedRequest);
            };

            okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(requestInterceptor)
                    .addInterceptor(httpLoggingInterceptor)
                    .build();
        }
        return okHttpClient;
    }

    public static BankAccountService getBankAccountServiceInstance() {
        if (bankAccountService == null) {
            bankAccountService = getRetrofitInstance().create(BankAccountService.class);
        }
        return bankAccountService;
    }
}
