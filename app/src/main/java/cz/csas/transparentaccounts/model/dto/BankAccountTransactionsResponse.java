package cz.csas.transparentaccounts.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import cz.csas.transparentaccounts.model.entity.BankAccountTransaction;

/**
 * Created by jankristdev@gmail.com on 1.10.2016.
 */

public class BankAccountTransactionsResponse extends ApiResponse {

    @SerializedName("transactions")
    private List<BankAccountTransaction> bankAccountTransactionList;

    public List<BankAccountTransaction> getBankAccountTransactionList() {
        return bankAccountTransactionList;
    }

    public void setBankAccountTransactionList(List<BankAccountTransaction> bankAccountTransactionList) {
        this.bankAccountTransactionList = bankAccountTransactionList;
    }
}
