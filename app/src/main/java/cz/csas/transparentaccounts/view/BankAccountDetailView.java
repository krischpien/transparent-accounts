package cz.csas.transparentaccounts.view;

import java.util.List;

import cz.csas.transparentaccounts.model.entity.BankAccount;
import cz.csas.transparentaccounts.model.entity.BankAccountTransaction;

/**
 * Created by jankristdev@gmail.com on 1.10LoadingView.2016.
 */

public interface BankAccountDetailView extends LoadingView {

    void renderBankAccount(BankAccount bankAccount);

    void renderTransactions(List<BankAccountTransaction> bankAccountTransactions);
}
