package cz.csas.transparentaccounts.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import cz.csas.transparentaccounts.BankApplication;
import cz.csas.transparentaccounts.R;
import cz.csas.transparentaccounts.adapter.AccountAdapter;
import cz.csas.transparentaccounts.model.entity.BankAccount;
import cz.csas.transparentaccounts.presenter.BankAccountListPresenter;
import cz.csas.transparentaccounts.view.BankAccountListView;
import cz.csas.transparentaccounts.view.fragment.BankAccountDetailFragment;

public class BankAccountListActivity extends AppCompatActivity implements BankAccountListView {

    private Toolbar toolbar;
    private RecyclerView accountRecyclerView;
    private ProgressBar progressBar;
    private boolean twoPaneMode = false;

    private SharedPreferences sharedPreferences;
    private BankAccountListPresenter presenter;
    private AccountAdapter accountAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_account_list);
        sharedPreferences = getSharedPreferences(getString(R.string.shared_preferences_main), Context.MODE_PRIVATE);

        setupPresenter();
        setupViews();
        detectPaneMode();
        setupToolbar();
        setupRecyclerViewWithAdapter();

        presenter.loadBankAccountList(0);
    }

    private void setupPresenter() {
        presenter = new BankAccountListPresenter(this);
        int appMode = sharedPreferences.getInt(getString(R.string.shared_preferences_key_application_mode), BankApplication.MODE_PRODUCTION);
        presenter.setAppMode(appMode);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_bank_account_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
        int appMode = presenter.getAppMode();
        switch (item.getItemId()) {
            case R.id.action_mode_mock:
                if(appMode == BankApplication.MODE_MOCK){
                    return true;
                }
                preferencesEditor.putInt(getString(R.string.shared_preferences_key_application_mode), BankApplication.MODE_MOCK);
                Toast.makeText(this, "Mock mode selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            case R.id.action_mode_production:
                if(appMode == BankApplication.MODE_PRODUCTION){
                    return true;
                }
                preferencesEditor.putInt(getString(R.string.shared_preferences_key_application_mode), BankApplication.MODE_PRODUCTION);
                Toast.makeText(this, "Production mode selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            default:
                break;
        }
        preferencesEditor.apply();
        restartActivity();
        return true;
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    private void setupViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        accountRecyclerView = (RecyclerView) findViewById(R.id.account_list);
        progressBar = (ProgressBar) findViewById(R.id.loading_progress_bar);
    }

    private void detectPaneMode() {
        if (findViewById(R.id.account_detail_container) != null) {
            twoPaneMode = true;
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
    }

    private void setupRecyclerViewWithAdapter() {
        accountAdapter = new AccountAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        accountRecyclerView.setLayoutManager(layoutManager);
        accountRecyclerView.setAdapter(accountAdapter);
        accountAdapter.setOnBankAccountClickListener(
                bankAccount -> showBankAccountDetail(bankAccount.getAccountNumber())
        );
        presenter.appendScrollLoadingListener(accountRecyclerView);

    }

    @Override
    public void renderBankAccountList(List<BankAccount> bankAccountList) {
        for (BankAccount bankAccount : bankAccountList) {
            accountAdapter.addBankAccountAndNotifyItemInserted(bankAccount);
        }
//        accountAdapter.addBankAccountListAndNotifyDataSetChanged(bankAccountList);
    }

    @Override
    public void showBankAccountDetail(String bankAccountId) {
        Bundle arguments = new Bundle();
        arguments.putString(BankAccountDetailFragment.ARG_ACCOUNT_ID, bankAccountId);
        if (twoPaneMode) {
            BankAccountDetailFragment bankAccountDetailFragment = new BankAccountDetailFragment();
            bankAccountDetailFragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.account_detail_container, bankAccountDetailFragment)
                    .commit();
        } else {
            Intent intent = new Intent(this, BankAccountDetailActivity.class);
            intent.putExtras(arguments);
            startActivity(intent);
        }

    }

    @Override
    public void showLoading(boolean loading) {
        int progressBarVisibility = loading ? View.VISIBLE : View.GONE;
        progressBar.setVisibility(progressBarVisibility);
    }
}
