package cz.csas.transparentaccounts.view;

/**
 * Created by jankristdev@gmail.com on 1.10.2016.
 */

public interface LoadingView extends BaseView {

    /**
     * Show loading view (progress bar, dialog etc..) while doing background work
     * @param loading
     */
    void showLoading(boolean loading);
}
