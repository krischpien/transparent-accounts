package cz.csas.transparentaccounts.view;

import java.util.List;

import cz.csas.transparentaccounts.model.entity.BankAccount;

/**
 * Created by jankristdev@gmail.com on 1.10.2016.
 */

public interface BankAccountListView extends LoadingView {

    /**
     * Show bank account detail as creating new activity or replacing
     * fragment (in case of two pane mode)
     * @param bankAccountId id of bank account to display
     */
    void showBankAccountDetail(String bankAccountId);

    void renderBankAccountList(List<BankAccount> bankAccountList);
}
