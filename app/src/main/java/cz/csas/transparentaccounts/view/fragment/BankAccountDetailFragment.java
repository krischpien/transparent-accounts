package cz.csas.transparentaccounts.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import cz.csas.transparentaccounts.BankApplication;
import cz.csas.transparentaccounts.R;
import cz.csas.transparentaccounts.model.entity.BankAccount;
import cz.csas.transparentaccounts.model.entity.BankAccountTransaction;
import cz.csas.transparentaccounts.presenter.BankAccountDetailPresenter;
import cz.csas.transparentaccounts.presenter.BankAccountListPresenter;
import cz.csas.transparentaccounts.utils.TimeUtils;
import cz.csas.transparentaccounts.view.BankAccountDetailView;
import cz.csas.transparentaccounts.view.activity.BankAccountDetailActivity;
import cz.csas.transparentaccounts.view.activity.BankAccountListActivity;
import timber.log.Timber;

/**
 * A fragment representing a single BankAccount detail screen.
 * This fragment is either contained in a {@link BankAccountListActivity}
 * in two-pane mode (on tablets) or a {@link BankAccountDetailActivity}
 * on handsets.
 * <p>
 * Created by jankristdev@gmail.com on 1.10.2016.
 */
public class BankAccountDetailFragment extends Fragment implements BankAccountDetailView {
    /**
     * The fragment argument representing the bankAccount ID that this fragment
     * represents.
     */
    public static final String ARG_ACCOUNT_ID = "item_id";
    private String accountId = null;

    private ViewGroup accountContent;
    private ViewGroup accountTransactionContainer;
    private TextView accountNumberText;
    private TextView accountBalanceText;
    private TextView accountTransparencyFromText;
    private TextView accountTransparencyToText;

    private ProgressBar progressBar;
    private BankAccountDetailPresenter presenter;
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.shared_preferences_main), Context.MODE_PRIVATE);
        setupPresenter();
        handleArguments();
    }

    private void setupPresenter() {
        presenter = new BankAccountDetailPresenter(this);
        int appMode = sharedPreferences.getInt(getString(R.string.shared_preferences_key_application_mode), BankApplication.MODE_PRODUCTION);
        presenter.setAppMode(appMode);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bank_account_detail, container, false);
        accountNumberText = (TextView) rootView.findViewById(R.id.account_detail_number);
        accountBalanceText = (TextView) rootView.findViewById(R.id.account_detail_balance);
        accountTransparencyFromText = (TextView) rootView.findViewById(R.id.account_detail_transparency_from);
        accountTransparencyToText = (TextView) rootView.findViewById(R.id.account_detail_transparency_to);
        accountContent = (ViewGroup) rootView.findViewById(R.id.account_detail_content);
        accountTransactionContainer = (ViewGroup) rootView.findViewById(R.id.account_transaction_container);
        progressBar = (ProgressBar) rootView.findViewById(R.id.loading_progress_bar);
        return rootView;
    }

    private void handleArguments() {
        if (getArguments().containsKey(ARG_ACCOUNT_ID)) {
            accountId = getArguments().getString(ARG_ACCOUNT_ID, null);
        }
    }


    @Override
    public void renderBankAccount(BankAccount bankAccount) {
        Timber.d("loaded account: %s", bankAccount);
        String fullBankAccountNumberString = getString(R.string.account_detail_number, bankAccount.getAccountNumber(), bankAccount.getBankCode());
        String fullBalanceString = getString(R.string.account_detail_balance, bankAccount.getBalance(), bankAccount.getCurrency());

        accountNumberText.setText(fullBankAccountNumberString);
        accountBalanceText.setText(fullBalanceString);
        accountTransparencyFromText.setText(TimeUtils.formatDate(bankAccount.getTransparencyFrom()));
        accountTransparencyToText.setText(TimeUtils.formatDate(bankAccount.getTransparencyTo()));
    }

    @Override
    public void renderTransactions(List<BankAccountTransaction> bankAccountTransactions) {
        for (BankAccountTransaction bankAccountTransaction : bankAccountTransactions) {
            appendTransactionLine(bankAccountTransaction);
        }
    }

    private void appendTransactionLine(BankAccountTransaction bankAccountTransaction) {
        View transactionView = LayoutInflater.from(getContext()).inflate(R.layout.item_transaction, accountContent, false);
        TextView valueText = (TextView) transactionView.findViewById(R.id.account_transaction_value);
        TextView senderReceiverText = (TextView) transactionView.findViewById(R.id.account_transaction_sender_receiver);
        TextView accountTransactionDateText = (TextView) transactionView.findViewById(R.id.account_transaction_date);
        senderReceiverText.setText(getString(R.string.account_transaction_sender_receiver,
                bankAccountTransaction.getSender().getAccountNumber(),
                bankAccountTransaction.getSender().getBankCode(),
                bankAccountTransaction.getReceiver().getAccountNumber(),
                bankAccountTransaction.getReceiver().getBankCode()));
        valueText.setText(getString(R.string.account_transaction_value,
                bankAccountTransaction.getAmount().getValue(),
                bankAccountTransaction.getAmount().getCurrency()));
        accountTransactionDateText.setText(TimeUtils.formatDate(bankAccountTransaction.getProcessingDate()));
        accountContent.addView(transactionView);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.loadAccountDetail(accountId);
    }

    @Override
    public void showLoading(boolean loading) {
        int progressBarVisibility = loading ? View.VISIBLE : View.GONE;
        int contentVisibility = loading ? View.GONE : View.VISIBLE;
        progressBar.setVisibility(progressBarVisibility);
        accountContent.setVisibility(contentVisibility);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.onStop();
    }
}
