package cz.csas.transparentaccounts.view.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import cz.csas.transparentaccounts.R;
import cz.csas.transparentaccounts.view.fragment.BankAccountDetailFragment;

/**
 * Created by jankristdev@gmail.com on 01.10.2016.
 */

public class BankAccountDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_account_detail);
        setupToolbar();
        if (savedInstanceState == null) {
            showDetailFragment();
        }
    }

    private void showDetailFragment() {
        Bundle arguments = new Bundle();
        arguments.putString(BankAccountDetailFragment.ARG_ACCOUNT_ID,
                getIntent().getStringExtra(BankAccountDetailFragment.ARG_ACCOUNT_ID));
        BankAccountDetailFragment fragment = new BankAccountDetailFragment();
        fragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.account_detail_container, fragment)
                .commit();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}