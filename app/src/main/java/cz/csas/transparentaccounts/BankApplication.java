package cz.csas.transparentaccounts;

import android.app.Application;

import timber.log.Timber;

public class BankApplication extends Application {

    public static final int MODE_PRODUCTION = 0;
    public static final int MODE_MOCK = 1;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
